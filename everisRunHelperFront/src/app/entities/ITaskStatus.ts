export interface ItaskStatus {
  "idProcess": string,
  "task": string,
  "status": string,
  "detail": string,
  "create": string,
  "update": string,
  "response": string
}
