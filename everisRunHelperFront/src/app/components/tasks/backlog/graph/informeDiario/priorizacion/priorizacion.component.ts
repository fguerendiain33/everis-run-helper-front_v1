import { Component, OnInit, Input } from '@angular/core';
import { ChartType, ChartDataSets, ChartOptions } from 'chart.js';
import { Label } from 'ng2-charts';
import * as pluginDataLabels from 'chartjs-plugin-datalabels';

@Component({
  selector: 'app-priorizacion',
  templateUrl: './priorizacion.component.html',
  styleUrls: ['./priorizacion.component.css']
})
export class PriorizacionComponent implements OnInit {

  @Input() backlogData;
  @Input() barColors;

  public dataArrayBaja: Array<any> = [];
  public dataArrayAlta: Array<any> = [];
  public dataArrayUrgente: Array<any> = [];
  public dataLabels: Array<any> = [];

  public barChartLabels: Label[];
  public barChartType: ChartType = 'bar';
  public barChartLegend = true;
  public barChartData: ChartDataSets[];



  public barChartOptions: ChartOptions = {
  //  responsive: true,
    maintainAspectRatio: false,
    scales: {
      xAxes: [
        {
          gridLines: {
            display: false
          },
          ticks: {
            beginAtZero: true
          }
        }
      ],
      yAxes: [
        {
          gridLines: {
            lineWidth: 0,
            zeroLineWidth: 1,
            zeroLineColor: 'grey'
          },
          ticks: {
            beginAtZero: true,
            stepSize: 20,
            max: 100
          }
        }
      ]
    },
    legend: {
      position: 'bottom',
      labels: {
        fontSize: 10,
        usePointStyle: true
      }
    },
    plugins: {
      datalabels: {
        formatter: (value, context) => {
          return '%' + value;
        },
        color: 'black',
        display: (context) => {
          return context.dataset.data[context.dataIndex] > 0;
        }
      }
    }
  };

  public barChartPlugins = [pluginDataLabels];



  constructor() {}


  ngOnInit() {
    let bajaAux: number;
    let altaAux: number;
    let urgenteAux: number;
    let creadasAux: string;
    let resueltasAux: string;
    let fechaAux: string;
    let total: number;
    let condicionDeIteracion: number;

    condicionDeIteracion = 1;
    let today: Date = new Date();
    if (today.getDay() === 1) {
      condicionDeIteracion = 0;
      today = new Date(new Date().setDate(new Date().getDate() - 1));
    }
    const dd: string = String(today.getDate()).padStart(2, '0');
    const mm: string = String(today.getMonth() + 1).padStart(2, '0');
    const todayFormated: string = dd + '/' + mm;

    for (let i = (this.backlogData.datosUltimosDiasArray).length - 1 ; i >= condicionDeIteracion; i--) {
      fechaAux = this.formatDate(this.backlogData.datosUltimosDiasArray[i].fecha);

      if (fechaAux !== todayFormated) {

        creadasAux = this.backlogData.datosUltimosDiasArray[i].creadas;
        resueltasAux = this.backlogData.datosUltimosDiasArray[i].resueltas;

        bajaAux = Math.trunc(this.backlogData.datosUltimosDiasArray[i].bajaPriorizadoPorcentaje * 100);
        altaAux = Math.trunc(this.backlogData.datosUltimosDiasArray[i].altaPriorizadoPorcentaje * 100);
        urgenteAux = Math.trunc(this.backlogData.datosUltimosDiasArray[i].urgentePriorizadoPorcentaje * 100);

        if (creadasAux !== '0' && resueltasAux !== '0') {
          total = bajaAux + altaAux + urgenteAux;
          if ( total < 100) {
              bajaAux = bajaAux + (100 - total);
          }
          this.dataLabels.push(fechaAux);
          this.dataArrayBaja.push(bajaAux);
          this.dataArrayAlta.push(altaAux);
          this.dataArrayUrgente.push(urgenteAux);
        }
        if ((this.dataLabels.length) > 6) {
          this.dataLabels.shift();
          this.dataArrayBaja.shift();
          this.dataArrayAlta.shift();
          this.dataArrayUrgente.shift();
        }
      }
    }

    this.barChartData = [
      {
        data: this.dataArrayBaja,
        label: 'baja',
        backgroundColor: this.barColors.greenBackgroundColor,
        borderColor: this.barColors.greenBorderColor,
        hoverBackgroundColor: this.barColors.greenHoverBackgroundColor,
        hoverBorderColor: this.barColors.greenHoverBorderColor,
        stack: 'a',
        barPercentage: 0.4,
        categoryPercentage: 0.9
      },
      {
        data: this.dataArrayAlta,
        label: 'media',
        backgroundColor: this.barColors.yellowBackgroundColor,
        borderColor: this.barColors.yellowBorderColor,
        hoverBackgroundColor: this.barColors.yellowHoverBackgroundColor,
        hoverBorderColor: this.barColors.yellowHoverBorderColor,
        stack: 'a',
        barPercentage: 0.4,
        categoryPercentage: 0.9
      },
      {
        data: this.dataArrayUrgente,
        label: 'alta',
        backgroundColor: this.barColors.redBackgroundColor,
        borderColor: this.barColors.redBorderColor,
        hoverBackgroundColor: this.barColors.redHoverBackgroundColor,
        hoverBorderColor: this.barColors.redHoverBorderColor,
        stack: 'a',
        barPercentage: 0.4,
        categoryPercentage: 0.9
      },

    ];
    this.barChartLabels = this.dataLabels;
  }

  formatDate(date: string): string {
    const dia: string = date.substr(6, 2);
    const mes: string = date.substr(4, 2);
    return dia + '/' + mes;
  }
}
